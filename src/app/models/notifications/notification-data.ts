export class NotificationData {
    success     : boolean;
	recordsTotal: number;
	page        : number;
	count       : number;
	Items       : Notification[];
}
