import { Component, OnInit } from '@angular/core';
import {UserLogin} from '../../models/UserLogin';
import {ActivatedRoute, Router} from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SeguridadService } from 'src/app/services/seguridad.service';
import { MatRadioChange } from '@angular/material/radio';
import { KeycodeCaptcha, MAXINTENT } from 'src/app/shared/constantes';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  sitekey  = "";
  intent : number = 0;
  RequerdCaptcha :boolean = true;
  captchaView : boolean;
  mensaje: string ="";
  doctype ="dni";
  getMaxLengthNumeroDocumento: number = 8;
  getMin : number = 8;
  placeHolder : string ="Número de DNI";
  cont : boolean =true;
  TOkenCaptcha :string ="";
  Formulario: FormGroup;
  RequestUser: UserLogin = new UserLogin();
  constructor(private fb: FormBuilder ,
    private securityService : SeguridadService,
    private router: Router,
    private route: ActivatedRoute
    ) { 
      this.sitekey =KeycodeCaptcha;
    }

  ngOnInit(): void {
    this.Formulario = this.fb.group({
      fm_option: this.fb.control('1'),
      fm_usuario: this.fb.control('', [ Validators.pattern('^[0-9]+$') ]),
      fm_pass: this.fb.control('', [Validators.required]),
      recaptchaReactive: this.fb.control(''),
    });
  }

  radioChange($event: MatRadioChange) {
    this.Formulario.get("fm_usuario").setValue('');
    this.Formulario.get("fm_pass").setValue('');
    const tipodoc = $event.value;

    if (tipodoc === '1') {
      this.doctype ="dni";
      this.placeHolder = "Número de DNI"
      this.getMaxLengthNumeroDocumento = 8;
      this.getMin = 8;
    }else if (tipodoc === '2'){
      this.doctype ="ce";
      this.placeHolder = "Número de CE"
      this.getMaxLengthNumeroDocumento = 12;
      this.getMin = 8;

    }
}

  formInvalid(control: string) {
   
    return this.Formulario.get(control).invalid &&
      (this.Formulario.get(control).dirty || this.Formulario.get(control).touched) ;
  }

  loginInit() {
    this.mensaje= "";
    this.RequestUser.docType = this.doctype;
    this.RequestUser.doc = this.Formulario.controls['fm_usuario'].value;
    this.RequestUser.password = this.Formulario.controls['fm_pass'].value;
    this.RequestUser.recaptcha = this.TOkenCaptcha;

    this.securityService.GetLogin<any>(this.RequestUser)
    .subscribe(
      data => {
        if (data.success){
         sessionStorage.setItem('accessToken', data.token);
         if(data.updated_password){
          this.router.navigate(['/main']);
        }else{
          this.router.navigate(['/nueva-contrasena']);
        }
        }else{
          this.intent ++;
        if(this.intent >= MAXINTENT){
          this.RequerdCaptcha =true;
        }
         this.mensaje = data.error.message;
        }
      },
      error => {
        
        this.mensaje= "Error de servicio , intente de nuevo o mas tarde.";
      
      }
    );

  }


  forgetpass(){

     this.router.navigate(['/recuperar-contrasena']);

  }

  validar_campo(event): boolean{
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
   }
   resolved(captchaResponse: string) {
    this.TOkenCaptcha = captchaResponse;
  }

}
