import { Filters } from './../../../models/notifications/notification';
import { Component, OnInit } from '@angular/core';
import { NotificationData } from 'src/app/models/notifications/notification-data';
import { NotificationRequest } from 'src/app/models/notifications/notification-request';
import { NotificationService } from 'src/app/services/notification.service';
import { PageEvent } from '@angular/material/paginator';
import { FuncionesService } from 'src/app/utils/funciones.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-notificaciones',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  notificationRequest: NotificationRequest;
  notificationData: NotificationData;
  listReadyCheck: boolean;
  textSearch: string = '';
  filterSelected: string = '0';

  constructor(private notificationService: NotificationService, private router: Router, private funcionesService: FuncionesService) {
    this.notificationData = new NotificationData();
  }

  ngOnInit(): void {
    this.loadNotifications('', 1, 5);
  }

  loadNotifications(querySearch: string, page ?: number, pageSize ?: number) {
    this.listReadyCheck = false;
    this.notificationRequest = new NotificationRequest();
    this.notificationRequest.search = querySearch;
    this.notificationRequest.filter = this.filterSelected.toString();
    this.notificationRequest.page = page;
    this.notificationRequest.count = pageSize;
    this.notificationService.GetNotifications(this.notificationRequest).subscribe(res => {
      if (res.success) {
        this.listReadyCheck = true;
        this.notificationData  = res;
      }
    }, err => {
      console.log('Problemas del servicio', err);
    });
  }

  filters: Filters [] = [
    { id: '0', value: 'Todos' },
    { id: '1', value: 'Leído' },
    { id: '2', value: 'No Leído' }
  ];

  getColor(name: string){
    return this.funcionesService.colorLetter(name);
  }
  searchByQuery() {
    this.loadNotifications(this.textSearch, 1, 5);
  }
  
  pageChangeEvent(event) {
    this.loadNotifications('', event.pageIndex + 1, event.pageSize);
  }

  goNotificationDetail(item: any) {
		this.router.routeReuseStrategy.shouldReuseRoute = () => false;
		this.router.navigate(['/main/notificaciones-detalle/' + item]);
	}

}



