import { FuncionesService } from '../../../utils/funciones.service';
import { Component, HostListener, OnInit } from '@angular/core';
import { Procedure, TypeDocument, ModeloResponse} from '../../../models/notifications/notification';
import { SendNotification } from 'src/app/models/notifications/notification';
import { SendNotificationRequest } from 'src/app/models/notifications/notification-request';
import { NotificationService } from 'src/app/services/notification.service';
import { FormControl, FormGroup } from '@angular/forms';
import { FileUploadControl, FileUploadValidators } from '@iplab/ngx-file-upload';
import { ResultSignature } from 'src/app/shared/constantes';
import { Router } from '@angular/router';

declare var initInvoker: any;
declare var dispatchEventClient: any;
@Component({
  selector: 'app-nueva-notificacion',
  templateUrl: './new-notification.component.html',
  styleUrls: ['./new-notification.component.scss']
})
export class NewNotificationComponent implements OnInit {

  sendNotification: SendNotificationRequest = new SendNotificationRequest();
  notification: SendNotification = new SendNotification();
  addressee: string = '';
  documentTypeSelected: string = '';
  maxlengthNumDoc: number;
  getMin : number 
  sectionOne: boolean = true;
  sectionTree: boolean = false;
  buttonNext: boolean = true;
  buttonSend: boolean = false;
  listProcedure: Procedure[];
  procedureSelected: string = '';
  procedureSelectedValue: string;
  inputDisabled: boolean = true;
  
  placeHolder ="Ingrese número de documento";

  parametro :string;

  multiple: boolean = false;
  animation: boolean = true;

  uploadedFiles: Array<File> = [];

  modeloResponse: ModeloResponse;
  public fileUploadControl = new FileUploadControl(FileUploadValidators.filesLimit(2));

  constructor(private notificationService: NotificationService,private router: Router, private funcionesService: FuncionesService) {

     //TODO:Events Signature
     window.addEventListener('invokerOk', (event:any) => {
      var type = event.detail;
      if (type === ResultSignature.TypeEvenReniec) {
          this.saveNotification();
        //send Notification api
        console.log("--InvokerSuccess()");
      }
    });
    window.addEventListener('getArguments', (event:any) => {
      var type = event.detail;
      if (type === ResultSignature.TypeEvenReniec) {
        console.log("--InvokerSend()");
        dispatchEventClient('sendArguments', this.parametro);
      }
    });
    window.addEventListener('invokerCancel', (event) => {
      console.log("--invokerCancel()");
      this.funcionesService.mensajeError('El proceso de firma digital fue cancelado.');
    });


   }

  ngOnInit(): void {
    this.getProcedure();
  }


  typeDocument: TypeDocument[] = [
    {id: '', value: 'Seleccione'},
    {id: 'dni', value: 'DNI'},
    {id: 'otro', value: 'Carnet de Extranjería'}
  ];

  getProcedure() {
    this.notificationService.GetProcedure().subscribe(res => {
      if (res.success){
        this.listProcedure = res.data.procedures;
        this.listProcedure.push({code: '', value: 'Seleccione'});
      }
    }, err => {
      console.log('Problemas del servicio', err);
    });
  }

  ConsultPerson() {
    this.addressee = '';
    if (this.documentTypeSelected !== '') {
      let personRequest: any = { "docType": this.documentTypeSelected, "doc": this.notification.doc };
      this.notificationService.ConsultPerson(personRequest).subscribe(res => {
        if (res.success){
          this.addressee = res.person;
          this.inputDisabled = this.addressee !== '' ? false : true;
        }else{
          this.funcionesService.mensajeError(res.error.message + ' ' + this.notification.doc);
          this.inputDisabled = true;
        }
      }, err => {
        console.log('Problemas del servicio', err);
      });
    }else {
      this.funcionesService.mensajeError('Debe seleccionar un tipo de documento');
    }
  }

  changeTypeDocument(){
    this.notification.doc = '';
    this.addressee = '';
    if (this.documentTypeSelected === 'dni'){
      this.maxlengthNumDoc = 8;
      this.getMin = 8;
      this.placeHolder ="Ingrese número de DNI";
    }else {
      this.maxlengthNumDoc = 12;
      this.getMin = 8;
      this.placeHolder ="Ingrese número de CE";
    }
  }

  changeProcedure(event){
    this.procedureSelectedValue = event.source.triggerValue;
  }

NotificationSign(){
  const formDataNotification = new FormData();
  formDataNotification.append('docType', this.documentTypeSelected);
  formDataNotification.append('doc', this.notification.doc);
  formDataNotification.append('name', this.addressee);
  formDataNotification.append('expedient', this.notification.expedient);
  formDataNotification.append('message', this.notification.message);
  if (this.uploadedFiles.length > 0) {
    if (this.uploadedFiles[0]) {
      formDataNotification.append('file1', this.uploadedFiles[0]);
    }
    if (this.uploadedFiles[1]) {
      formDataNotification.append('file2', this.uploadedFiles[1]);
    }
    if (this.uploadedFiles[2]) {
      formDataNotification.append('file3', this.uploadedFiles[2]);
    }
    if (this.uploadedFiles[3]) {
      formDataNotification.append('file4', this.uploadedFiles[3]);
    }
    if (this.uploadedFiles[4]) {
      formDataNotification.append('file5', this.uploadedFiles[4]);
    }
  }
  this.notificationService.GetNotificationSign(formDataNotification).subscribe(res => {
    if (res.success) {
      this.parametro = res.param;
      if (this.parametro.length > 0){
        initInvoker(ResultSignature.TypeEvenReniec);
      }else{
        this.funcionesService.mensajeError("No hay data para envío invoker");
      }
    }else {
      this.funcionesService.mensajeError(res.error.message);
    }
  }, err => {
    console.log('Problemas del servicio', err);
  });
}

  saveNotification() {

        const formDataNotification = new FormData();
        formDataNotification.append('docType', this.documentTypeSelected);
        formDataNotification.append('doc', this.notification.doc);
        formDataNotification.append('name', this.addressee);
        formDataNotification.append('expedient', this.notification.expedient);
        formDataNotification.append('message', this.notification.message);
        if (this.uploadedFiles.length > 0) {
          if (this.uploadedFiles[0]) {
            formDataNotification.append('file1', this.uploadedFiles[0]);
          }
          if (this.uploadedFiles[1]) {
            formDataNotification.append('file2', this.uploadedFiles[1]);
          }
          if (this.uploadedFiles[2]) {
            formDataNotification.append('file3', this.uploadedFiles[2]);
          }
          if (this.uploadedFiles[3]) {
            formDataNotification.append('file4', this.uploadedFiles[3]);
          }
          if (this.uploadedFiles[4]) {
            formDataNotification.append('file5', this.uploadedFiles[4]);
          }
        }
        this.notificationService.SendNotification(formDataNotification).subscribe(res => {
          if (res.success) {
            this.clearData();
            this.funcionesService.mensajeOk('Los datos de notificación fueron registrados con éxito', '/main/notificaciones');
          }else {
            this.funcionesService.mensajeError(res.error.message);
          }
        }, err => {
          console.log('Problemas del servicio', err);
        });
  }

  validateInputs(): boolean {
    if (this.documentTypeSelected === '') {
      return false;
    }else if (this.notification.doc === '') {
      return false;
    }else if (this.addressee === '') {
      return  false;
    }else if (this.uploadedFiles.length === 0) {
      return  false;
    }else {
      return  true;
    }
  }

  validations(): ModeloResponse{

    this.modeloResponse = new ModeloResponse();
    let validinput = this.validateInputs();

    if (!validinput){
      this.modeloResponse.message = 'Debe completar los campos vacios';
      this.modeloResponse.success = false;
      return this.modeloResponse;
    }

    if (this.uploadedFiles.length > 5){
      this.modeloResponse.message = 'No puede haber mas de 5 archivos';
      this.modeloResponse.success = false;
      return this.modeloResponse;
    }

    if (this.uploadedFiles.length === 0){
      this.modeloResponse.message = 'Debe cargar mínimo 1 archivo';
      this.modeloResponse.success = false;
      return this.modeloResponse;
    }

    for (let i = 0; i < this.uploadedFiles.length; i++) {
      let fileSizeMb = (this.uploadedFiles[i].size / 1024);
      if (fileSizeMb > 272){
        this.modeloResponse.success = false ;
        this.modeloResponse.message = 'El archivo no debe pesar mas de 272 Kb';
        return this.modeloResponse;
      }
    }
    this.modeloResponse.success = true;
    this.modeloResponse.message = '';
    return this.modeloResponse;
  }

  clearData() {
    this.notification = new SendNotification();
    this.addressee = '';
    this.uploadedFiles = undefined;
  }

  cancel() {
   if (this.buttonSend){
    this.sectionOne = true;
    this.sectionTree = false;
    this.buttonNext = true;
    this.buttonSend = false;
  }else{
    this.router.navigate(['/main']);
  }

  }

  next() {
    this.modeloResponse = this.validations();
    if (!this.modeloResponse.success) {
      this.funcionesService.mensajeError(this.modeloResponse.message);
      this.sectionOne = true;
      this.buttonNext = true;
      this.buttonSend = false;
    } else {
      this.sectionOne = false;
      this.sectionTree = true;
      this.buttonNext = false;
      this.buttonSend = true;
    }
  }

  validar_campo(event): boolean{
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
   }

}
