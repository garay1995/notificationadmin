import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NotificationRequest } from 'src/app/models/notifications/notification-request';
import { attachment, notification, notificationRequest } from 'src/app/models/notifications/notification';
import { NotificationService } from 'src/app/services/notification.service';


@Component({
  selector: 'app-notification-detalle',
  templateUrl: './notification-detalle.component.html',
  styleUrls: ['./notification-detalle.component.scss']
})
export class NotificationDetalleComponent implements OnInit {

  notiRequest :notificationRequest = new notificationRequest(); 
  //notificationRequest : notificationsRequest = new notificationsRequest();
  id :string;
  public formulario: FormGroup;
  adjuntos : attachment[];
  view1 : string ='color_1 posUno';
 
  view2 : string ='color_2 posDos';
  icon2 :string ='stop_circle';

  @Input() numNotview :number;

  constructor(private fb: FormBuilder,private route: ActivatedRoute,private notificationsservices : NotificationService) {
   
    this.id = this.route.snapshot.paramMap.get('id');
   }

  ngOnInit(): void {
  this.createForm();
  this.GetNotificationDetail();

  }

  createForm():void{
    this.formulario = this.fb.group({
      n_expediente :[''],
      n_notifier_Area :[''],
      n_received_at :[''],
      n_read_at  :[''],
      n_message :['']
    });
  }


  GetNotificationDetail(){

    this.notiRequest.id= this.id;

    this.notificationsservices.getNotificationDetail<any>(this.notiRequest)
    .subscribe(
      data => {
        if (data.success){
          //this.notificationRespone = data;
          this.loadNotificationData(data.notification);
          this.view2  ='color_1 posDos';
          this.icon2  ='check_circle';
        }else{
         //this.mensaje=data.error.message;
        }
      },
      error => {
       // this.mensaje="Error de servicio , intente de nuevo o mas tarde.";
      }
    );
  }

 


  loadNotificationData(noti :notification){
    this.formulario.get("n_expediente").setValue(noti.expedient);
    this.formulario.get("n_notifier_Area").setValue(noti.notifier_area);
    this.formulario.get("n_received_at").setValue(noti.received_at);
    this.formulario.get("n_read_at").setValue(noti.read_at);
    this.formulario.get("n_message").setValue(noti.message);
    this.adjuntos = noti.attachments;
  }
  viewDocument(item : any){
    window.open(item.url, '_blank');
  }

  print(){
    let printContents = document.getElementById('imp1').innerHTML;
    let contenido= document.getElementById('imp1').innerHTML;
    let contenidoOriginal= document.body.innerHTML;
    document.body.innerHTML = contenido;
    window.print();
    document.body.innerHTML = contenidoOriginal;
    location.reload();
  }

}
