import { Component, OnInit } from '@angular/core';
import { UserData } from 'src/app/models/users/user-data';
import { UserRequest } from 'src/app/models/users/user-request';
import { UserService } from 'src/app/services/user.service';
import { FuncionesService } from 'src/app/utils/funciones.service';

interface Filtro {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-usuarios',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  userRequest: UserRequest;
  userData: UserData;
  filterSelected: string = '0';
  textSearch: string = '';
  listReadyCheck: boolean;

  constructor(private userService: UserService, private funcionesService: FuncionesService) {
    this.userData = new UserData();
   }

  ngOnInit(): void {
    this.loadUsers('', 1, 5);
  }
 
  loadUsers(uerySearch: string, page ?: number, pageSize ?: number) {
    this.listReadyCheck = false;
    this.userRequest = new UserRequest();
    this.userRequest.search = uerySearch;
    this.userRequest.page = page;
    this.userRequest.count = pageSize;
    this.userService.GetUsers(this.userRequest).subscribe(res => {
      if (res.success) {
        this.listReadyCheck = true;
        this.userData  = res;
      }
    }, err => {
      console.log('Problemas del servicio', err);
    });
  }
  searchByQuery() {
    this.loadUsers(this.textSearch, 1, 5);
  }

  getColor(name: string){
    return this.funcionesService.colorLetter(name);
  }

  pageChangeEvent(event) {
    this.loadUsers('', event.pageIndex + 1, event.pageSize);
  }
}
