import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TypeDocument } from 'src/app/models/notifications/notification';
import { Box, TypeAccreditation } from 'src/app/models/users/user';
import { BoxRequest } from 'src/app/models/users/user-request';
import { UserService } from 'src/app/services/user.service';
import { FuncionesService } from 'src/app/utils/funciones.service';

interface Filtro {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-nueva-casilla',
  templateUrl: './new-box.component.html',
  styleUrls: ['./new-box.component.scss']
})
export class NewBoxComponent implements OnInit {
  
  @ViewChild("fileUpload", {static: false}) fileUpload: ElementRef;

  boxRequest: BoxRequest = new BoxRequest();
  box: Box = new Box();
  typeAccreditationSelected: string = '';
  name: string = '';
  documentTypeSelected: string = "";
  maxlengthNumDoc: number;
  listTypeAcreditation: TypeAccreditation[];
  inputDisabled: boolean = false;
  placeHolder ="Ingrese número de documento";
  
  Formulario: FormGroup;

  fileLoad: any;
  public fileToUpload: File;

  constructor(private userService: UserService, private funcionesService: FuncionesService,private router: Router,private fb: FormBuilder) { }

  ngOnInit(): void {
    this.Formulario = this.fb.group({
      fm_optiontipo: this.fb.control(''),
      fm_numerodoc: this.fb.control('', [ Validators.pattern('^[0-9]+$') ]),
      fm_nombreape: this.fb.control({value: '', disabled: this.inputDisabled}),
      fm_correo: this.fb.control({value: '', disabled: this.inputDisabled}, [Validators.required,Validators.pattern('[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')]),
      fm_celular: this.fb.control({value: '', disabled: this.inputDisabled}, [Validators.required]),
      fm_fijo: this.fb.control({value: '', disabled: this.inputDisabled}, [Validators.required, Validators.maxLength(10)] ),
      fm_direccion: this.fb.control({value: '', disabled: this.inputDisabled}, [Validators.required]),      
      fm_optiontacreditacion: this.fb.control({value: '', disabled: this.inputDisabled}),


    });


    this.getTypeAcreditacion();
  }

  typeDocument: TypeDocument[] = [
    {id: '', value: 'Seleccione'},
    {id: 'dni', value: 'DNI'},
    {id: 'ce', value: 'Carnet de Extranjería'}
  ];

  getTypeAcreditacion(){
    this.userService.GetTypeAcreditation().subscribe(res => {
      if (res.success) {
        this.listTypeAcreditation = res.data.acreditationTypes;
        this.listTypeAcreditation.push({code : '', value: 'Seleccione'});
      }
    }, err => {
      console.log('Problemas del servicio', err);
    });
  }

  changeTypeDocument(event){
    this.documentTypeSelected = event.value;
    this.box.doc = '';
    this.name = '';
    if (this.documentTypeSelected === 'dni'){
      this.maxlengthNumDoc = 8;
      this.placeHolder ="Ingrese número de DNI";
    }else {
      this.maxlengthNumDoc = 12;
      
      this.placeHolder ="Ingrese número de CE";
    }
  }
  formInvalid(control: string) {
    return this.Formulario.get(control).invalid &&
      (this.Formulario.get(control).dirty || this.Formulario.get(control).touched);
  }

  ConsultPerson() {
    this.name = '';
    if (this.documentTypeSelected !== '') {
      let personRequest: any = { "docType": this.documentTypeSelected, "doc": this.Formulario.controls['fm_numerodoc'].value };
      this.userService.ConsultPerson(personRequest).subscribe(res => {
        if (res.success){
          this.name = res.person.name;
          this.inputDisabled = false;
          this.enableForm();
        }else {
          this.funcionesService.mensajeError(res.error.message + ' ' + this.box.doc);
          this.inputDisabled = true;
        }
      }, err => {
        console.log('Problemas del servicio', err);
      });
    }else {
      this.funcionesService.mensajeError('Debe seleccionar un tipo de documento');
    }
  }

  enableForm(){

    this.Formulario.get('fm_correo').enable();
    this.Formulario.get('fm_celular').enable();
    this.Formulario.get('fm_fijo').enable();
    this.Formulario.get('fm_direccion').enable();
  }
  



  fileUploadchange(fileInput: any){
    this.fileToUpload = <File> fileInput.target.files[0];
    let fileSizeMb = 0;
    if(this.fileToUpload != undefined){
       fileSizeMb = fileInput.target.files[0].size;
    }

    if (fileSizeMb < 3000000) {
      if (fileInput.target.id === 'pdf_creation_solicitude'){
        this.box.pdf_creation_solicitude = this.fileToUpload;
      }else if (fileInput.target.id === 'pdf_resolution'){
        this.box.pdf_resolution = this.fileToUpload;
      }else if (fileInput.target.id === 'pdf_agree_tos'){
        this.box.pdf_agree_tos = this.fileToUpload;
      }
    }else {
      this.funcionesService.mensajeError('El archivo no debe ser mayor a 3 Mb');
      fileInput.target.value = '';
    }
   
  }

  saveBox() {
    if (this.validateInputs()) {
      const formDataBox = new FormData();
      formDataBox.append('docType', this.documentTypeSelected);
      formDataBox.append('doc', this.Formulario.controls['fm_numerodoc'].value);
      formDataBox.append('email', this.Formulario.controls['fm_correo'].value);
      formDataBox.append('cellphone', this.Formulario.controls['fm_celular'].value);
      formDataBox.append('phone', this.Formulario.controls['fm_fijo'].value);
      formDataBox.append('address', this.Formulario.controls['fm_direccion'].value);
      formDataBox.append('acreditation_type',this.Formulario.controls['fm_optiontacreditacion'].value);
      formDataBox.append('pdf_resolution', this.box.pdf_resolution );
      formDataBox.append('pdf_creation_solicitude', this.box.pdf_creation_solicitude);
      formDataBox.append('pdf_agree_tos', this.box.pdf_agree_tos);
      this.userService.CreateBox(formDataBox).subscribe(res => {
        if (res.success) {
          this.funcionesService.mensajeOk('Los datos de casilla electrónica fueron registrados con éxito', '/main/operador/usuarios');
        }
      }, err => {
        console.log('Problemas del servicio', err);
      });
    } else {
      this.funcionesService.mensajeError('Debe completar bien todos los campos');
    }
  }

  validateInputs() {
    let isValid: boolean = false;
    if (this.documentTypeSelected === '') {
      return isValid = false;
    }else if (this.Formulario.controls['fm_optiontacreditacion'].value === ''){
      return isValid = false;
    }else if (this.box.pdf_resolution === undefined){
      return isValid = false;
    }else if (this.box.pdf_creation_solicitude === undefined){
      return isValid = false;
    }else if (this.box.pdf_agree_tos === undefined){
      return isValid = false; 
    }else {
      return isValid = true;
    }
  }

  validar_campo(event): boolean{
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
   }

   cancelar(){
    this.router.navigate(['/main']);
   }

}
