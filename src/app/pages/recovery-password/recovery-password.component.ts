import { Component, OnInit } from '@angular/core';
import {recoverypass, UserLogin} from '../../models/UserLogin';
import {Router} from '@angular/router';
import { MatRadioChange } from '@angular/material/radio';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SeguridadService } from 'src/app/services/seguridad.service';
import { KeycodeCaptcha } from 'src/app/shared/constantes';
@Component({
  selector: 'app-recuperar-contrasena',
  templateUrl: './recovery-password.component.html',
  styleUrls: ['./recovery-password.component.scss']
})
export class RecoveryPasswordComponent implements OnInit {
  usuario: UserLogin = new UserLogin();
  mensaje: string;


  TOkenCaptcha :string ="";
  RequerdCaptcha :boolean = true;
  sitekey  = "";
  ModelRequestRecover : recoverypass = new recoverypass();
  Formulario: FormGroup;
  getMaxLengthNumeroDocumento: number = 8;
  getMin : number = 8;
  doctype :string = "dni";
  placeHolder : string ="Número de DNI";

  notView : boolean= false;
  msgCorrect ="En caso te encuentres registrado en SINONPE, te hemos enviado un correo con los pasos a seguir para recuperar tu contraseña. Por favor, verifica tu bandeja de entrada y correo de no deseados."

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private securityService : SeguridadService
  ) {
    this.mensaje = 'Ingrese su documento de identidad';
    this.sitekey =KeycodeCaptcha;
  }

  ngOnInit(): void {

    this.Formulario = this.fb.group({
      fm_option: this.fb.control('1'),
      fm_usuario: this.fb.control('', [ Validators.pattern('^[0-9]+$') ]),
      recaptchaReactive: this.fb.control('')
    
    });

  }
  onSumit() {
    this.ModelRequestRecover.docType = this.doctype;
    this.ModelRequestRecover.doc =  this.Formulario.controls['fm_usuario'].value;
    this.ModelRequestRecover.recaptcha = this.TOkenCaptcha;
    this.securityService.GetRecoveryPassword<any>(this.ModelRequestRecover)
    .subscribe(
      data => {
        if (data.success){
  
          this.notView = true;

        }else{
         this.mensaje = data.error.message;
        }
      },
      error => {
        this.mensaje= "Error de servicio , intente de nuevo o mas tarde.";
      }
    );
  }

  radioChange($event: MatRadioChange) {
    this.Formulario.get("fm_usuario").setValue('');
    const tipodoc = $event.value;

    if (tipodoc === '1') {
      this.doctype ="dni";
      this.placeHolder = "Número de DNI"
      this.getMaxLengthNumeroDocumento = 8;
      this.getMin=8;
    }else if (tipodoc === '2'){
      this.doctype ="ce";
      this.placeHolder = "Número de CE"
      this.getMaxLengthNumeroDocumento = 12;
      this.getMin=8;

    }
}




formInvalid(control: string) {
  return this.Formulario.get(control).invalid &&
    (this.Formulario.get(control).dirty || this.Formulario.get(control).touched);
}

validar_campo(event): boolean{
  const charCode = (event.which) ? event.which : event.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  }
  return true;
 }

 resolved(captchaResponse: string) {
  this.TOkenCaptcha = captchaResponse;
}

}
