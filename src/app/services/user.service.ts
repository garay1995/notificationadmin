import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { BoxRequest, UserRequest } from '../models/users/user-request';
import { UserData } from '../models/users/user-data';

const API_URL = environment.URL_SERVICES;

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  GetUsers(userRequest: UserRequest): Observable<UserData> {
    return this.http.post<any>(API_URL + '/users', userRequest, httpOptions)
    .pipe(map(res => res));
  }

  GetTypeAcreditation(): Observable<any> {
    return this.http.get(API_URL + '/cache-box', httpOptions)
    .pipe(map(res => res));
  }

  ConsultPerson(personRequest: any): Observable<any>{
    return this.http.post<any>(API_URL + '/person', personRequest, httpOptions)
    .pipe(map(res => res));
  }

  CreateBox(boxRequest: FormData): Observable<any> {
    return this.http.post<any>(API_URL + '/create-box', boxRequest)
    .pipe(map(res => res));
  }

}
