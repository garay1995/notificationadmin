import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MaterialModule} from './material/material.module';
import { LoginComponent } from './pages/login/login.component';
import { MainComponent } from './pages/main/main.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatExpansionModule} from '@angular/material/expansion';
import { RandompagesComponent } from './pages/randompages/randompages.component';
import { NotificationsComponent } from './pages/main/notifications/notifications.component';
import { NewNotificationComponent } from './pages/main/new-notification/new-notification.component';
import { JwtInterceptor } from './utils/jwt-interceptor';
import { ErrorInterceptor } from './utils/error-interceptor';
import { NewPasswordComponent } from './pages/new-password/new-password.component';
import { OperadorComponent } from './pages/main/operador/operador.component';
import { UsersComponent } from './pages/main/operador/users/users.component';
import { NewBoxComponent } from './pages/main/operador/new-box/new-box.component';
import { RecoveryPasswordComponent } from './pages/recovery-password/recovery-password.component';
import { ReactiveFormsModule, FormsModule  } from '@angular/forms';
import { FileUploadModule } from '@iplab/ngx-file-upload';
import { NotificationDetalleComponent } from './pages/main/notification-detalle/notification-detalle.component';

import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    HeaderComponent,
    FooterComponent,
    RandompagesComponent,
    NotificationsComponent,
    NewNotificationComponent,
    NewPasswordComponent,
    OperadorComponent,
    UsersComponent,
    NewBoxComponent,
    RecoveryPasswordComponent,
    NotificationDetalleComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule ,
    FileUploadModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FlexLayoutModule,
    MatSidenavModule,
    MatExpansionModule,
    RecaptchaModule,  //this is the recaptcha main module
    RecaptchaFormsModule, //this is the module for form incase form validation
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
